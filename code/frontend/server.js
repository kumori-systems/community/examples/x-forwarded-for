/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

const express = require('express')
const https = require('https')

var hostname = '0.0.0.0'
var port = process.env.HTTP_SERVER_PORT_ENV

let app = express()

app.get('/get-headers', (req, res) => {
  let result =
    "New get-headers request received." +
    "\nHeaders:" + JSON.stringify(req.headers) +
    "\nRemoteAddress:" + req.socket.remoteAddress
  console.log("--------------------------------")
  console.log(result)
  console.log("--------------------------------")
  res.send(result)
})

app.get('/send-to', (req, res) => {
  console.log('New send-to request received.')
  console.log("Query:", JSON.stringify(req.query))
  console.log("Headers:", JSON.stringify(req.headers))
  console.log("RemoteAddress:", req.socket.remoteAddress)
  let dstUrl = req.query.dsturl
  let dstPath = req.query.dstpath
  const options = {
    hostname: dstUrl,
    port: 443,
    path: `/${dstPath}`,
    method: 'GET',
    headers: {
      'x-forwarded-for': req.headers["x-forwarded-for"]
    }
  }
  https.get(options, (response) => {
    let data = ''
    response.on('data', (chunk) => {
      data += chunk
    })
    response.on('end', () => {
      res.send(data)
    })
  }).on('error', (error) => {
    console.log(`Error: ${error.message}`)
    res.send(`Error: ${error.message}`)
  })
})

app.get('/health', (req, res) => {
  res.send('OK')
})

app.use((req, res, next) => {
  res.status(404).send('Not found')
})

app.listen(port, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})

package deployment

import (
  s ".../service:service"
)

#Deployment: {
  name: "helloworlddep"
  artifact: s.#Artifact
  config: {
    parameter: {
      remoteaddressheader: "X-Real-IP"
      cleanxforwardedfor: true
    }
    resource: {
      servercert: certificate: "cluster.core/wildcard-test-kumori-cloud"
      serverdomain: domain: "xffdomain"
    }
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}

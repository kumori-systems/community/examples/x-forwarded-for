package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../frontend:component"
  i "kumori.systems/builtins/inbound:service"
)

#Artifact: {
  ref: name:  "service"
  description: {

    config: {
      parameter: {
        remoteaddressheader: string
        cleanxforwardedfor: bool
      }
      resource: {
        servercert: k.#Certificate
        serverdomain: k.#Domain
      }
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
      httpinbound: {
        artifact: i.#Artifact
        config: {
          parameter: {
            type: "https"
            remoteaddressheader: description.config.parameter.remoteaddressheader
            cleanxforwardedfor: description.config.parameter.cleanxforwardedfor
          }
          resource: {
            servercert: description.config.resource.servercert
            serverdomain: description.config.resource.serverdomain
          }
          resilience: description.config.resilience
        }
      }
    }

    srv: {} // No service channels: the inbound is included as part of the service

    connect: {
      // inbound => frontend
      cinbound: {
        as: "lb"
        from: httpinbound: "inbound"
        to: frontend: restapi: _
      }
    }
  }
}

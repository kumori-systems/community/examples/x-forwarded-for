#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="xffdep"
DOMAIN="xffdomain"
SERVICEURL="xff-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

# Resend parameters
DSTURL="xff-demo.test.kumori.cloud"
DSTPATH="get-headers"

KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/builtins/inbound
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "X-Forwarded-For service" \
    --wait 5m
  ;;

'deploy-all')
  $0 create-domain
  $0 deploy-service
  ;;

'update-service')
  ${KAM_CMD} service update -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Updating X-Forwarded-For service" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

'test1')
  curl https://${SERVICEURL}/get-headers
  ;;

'test2')
  curl -H "x-forwarded-for: 1.2.3.4" https://${SERVICEURL}/get-headers
  ;;

'kk')
  curl -H "x-forwarded-for: 1.1.1.1,2.2.2.2" https://${SERVICEURL}/get-headers
  ;;

'test3')
  curl https://${SERVICEURL}/send-to?"dsturl=${DSTURL}&dstpath=${DSTPATH}"
  ;;

'test4')
  curl -H "x-forwarded-for: 1.2.3.4" https://${SERVICEURL}/send-to?"dsturl=${DSTURL}&dstpath=${DSTPATH}"
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN --wait 5m
  ;;

'undeploy-all')
  $0 undeploy-service
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
  ;;

esac
